library(DiagrammeR)
library(tidyverse)

# Create the graph
graph <- 
  create_graph() %>%
  # Add nodes to the graph
  add_node(label = "app file"                ,node_aes = node_aes(shape = "circle")) %>%
  add_node(label = "main file"               ,node_aes = node_aes(shape = "circle")) %>%
  add_node(label = "table file"              ,node_aes = node_aes(shape = "circle")) %>%
  add_node(label = "chart file"              ,node_aes = node_aes(shape = "circle")) %>%
  add_node(label = "user file"               ,node_aes = node_aes(shape = "circle")) %>%
  add_node(label = "utility file"            ,node_aes = node_aes(shape = "circle")) %>%
  add_node(label = "ui component"            ,node_aes = node_aes(shape = "rectangle")) %>%
  add_node(label = "server component"        ,node_aes = node_aes(shape = "rectangle")) %>%
  # main app contents ----------------------------------------------------
  add_node(label = "page"                   ,node_aes = node_aes(shape = "rectangle")) %>%
  add_node(label = "header"                 ,node_aes = node_aes(shape = "rectangle")) %>%
  add_node(label = "user"                   ,node_aes = node_aes(shape = "rectangle")) %>%
  add_node(label = "sidebar"                ,node_aes = node_aes(shape = "rectangle")) %>%
  add_node(label = "body"                   ,node_aes = node_aes(shape = "rectangle")) %>%
  add_node(label = "control bar"            ,node_aes = node_aes(shape = "rectangle")) %>%
  # Utility file functions ------------------------------------------------
  add_node(label = "get_data"               ,node_aes = node_aes(shape = "rectangle")) %>%
  add_node(label = "plot_bar_graph"         ,node_aes = node_aes(shape = "rectangle")) %>%
  add_node(label = "render_table"           ,node_aes = node_aes(shape = "rectangle")) %>%
  add_node(label = "user mode"              ,node_aes = node_aes(shape = "rectangle")) %>%
  add_node(label = "ingest data"            ,node_aes = node_aes(shape = "rectangle")) %>%
  add_node(label = "manipulate data"        ,node_aes = node_aes(shape = "rectangle")) %>%
  add_node(label = "render plot"            ,node_aes = node_aes(shape = "rectangle")) %>%
  add_node(label = "filter data"            ,node_aes = node_aes(shape = "rectangle")) %>%
  # Add edges to the graph
  add_edge(from = "app file"                       ,to =  "main file") %>%
  add_edge(from = "main file"                      ,to =  "ui component") %>%
  add_edge(from = "main file"                      ,to =  "server component") %>%
  add_edge(from = "ui component"                   ,to =  "page") %>%
  add_edge(from = "ui component"                   ,to =  "header") %>%
  add_edge(from = "header"                         ,to =  "user") %>%
  add_edge(from = "ui component"                   ,to =  "sidebar") %>%
  add_edge(from = "ui component"                   ,to =  "body") |> 
  add_edge(from = "ui component"                   ,to =  "control bar") %>%
  add_edge(from = "server component"               ,to =  "get_data") %>%
  add_edge(from = "server component"               ,to =  "plot_bar_graph") |> 
  add_edge(from = "server component"               ,to =  "render_table") %>%
  add_edge(from = "server component"               ,to =  "user mode") %>%
  add_edge(from = "utility file"                   ,to =  "ingest data") %>%
  add_edge(from = "utility file"                   ,to =  "manipulate data") %>%
  add_edge(from = "utility file"                   ,to =  "render plot") %>%
  add_edge(from = "utility file"                   ,to =  "filter data") %>%
  add_edge(from = "table file"                     ,to =  "ui component") %>%
  add_edge(from = "table file"                     ,to =  "server component") %>%
  add_edge(from = "chart file"                     ,to =  "ui component") %>%
  add_edge(from = "chart file"                     ,to =  "server component") %>%
  add_edge(from = "user file"                      ,to =  "ui component") %>%
  add_edge(from = "user file"                      ,to =  "server component")

# Plot the graph
render_graph(graph)


